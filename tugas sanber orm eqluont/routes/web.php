<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\ProfileController;
use App\Models\Genre;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/Register', [AuthController::class, 'register']);
Route::post('/kirim', [AuthController::class, 'kirim']);



Route::get('/data-table', function(){
        return view('halaman.data-table');
});
Route::get('/table', function(){
    return view('halaman.table');
});

//agar tidak bisa masuk secara sembarangan kita pakai middleware dan masukan semua route yg ingin di securenya
Route::middleware(['auth'])->group(function () {
   
    //CRUD CAST
    
    //create 
    //route untuk mengarah ke form tambah CAST
    Route::get('/cast/create',[CastController::class,'create']);
    //route untuk menyimpan data ke database atau tambah ke database
    Route::post('/cast', [CastController::class,'store']);
    
    
    //read
    //route untuk mennampilkan semua data yang di database
    Route::get('/cast', [CastController::class, 'index']);
    //route untuk detail cast berdasarkan id
    Route::get('/cast/{cast_id}', [CastController::class,'show']);
    
    
    //update
    //route untuk mengarah ke form update berdasarkan id (route edit)
    Route::get('/cast/{cast_id}/edit', [CastController::class,'edit']);
    //route untuk Update data ke database berdasarkan id
    Route::put('/cast/{cast_id}', [CastController::class, 'update']);
    
    //delete
    //delete berdasarkan id
    Route::delete('/cast/{cast_id}',[CastController::class, 'destroy']);
    
    //CRUD GENRE
    //create
    Route::get('/genre/create', [GenreController::class, 'create']);
    Route::post('/genre', [GenreController::class,'store']);
    
    //read
    Route::get('/genre', [GenreController::class, 'index']);
    Route::get('/genre/{genre_id}', [GenreController::class,'show']);
    
    
    //update
    //route untuk mengarah ke form update berdasarkan id (route edit)
    Route::get('/genre/{genre_id}/edit', [GenreController::class,'edit']);
    //route untuk Update data ke database berdasarkan id
    Route::put('/genre/{genre_id}', [GenreController::class, 'update']);
    
    //delete
    //delete berdasarkan id
    Route::delete('/genre/{genre_id}',[GenreController::class, 'destroy']);

    Route::resource('profile', ProfileController::class)->only('index','update');
});


// CRUD FILM
Route::resource('film', FilmController::class);


Auth::routes();


