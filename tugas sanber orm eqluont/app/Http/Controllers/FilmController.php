<?php

namespace App\Http\Controllers;

use App\Models\Film;
use Illuminate\Http\Request;
use App\Models\Genre;
use File;
class FilmController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index','store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $film = Film::get();
       return view('film.index', ['film' =>$film]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $genre =  Genre::get();
        return view('film.create', ['genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpg,jpeg,png',
            'genre_id' => 'required'
        ]);
        
      
  
        $fileName = time().'.'.$request->poster->extension();  
   
        $request->poster->move(public_path('images'), $fileName);
     
            $film = new Film;
     
            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->genre_id = $request->genre_id;
            $film->poster = $fileName;
     
            $film->save();

            return redirect('/film');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $film = Film::find($id);
      return view('film.detail', ['film'=>$film]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::get();

        return view('film.update', ['film'=> $film, 'genre'=>$genre]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'image|mimes:jpg,jpeg,png',
            'genre_id' => 'required'
        ]);

        $film = Film::find($id);
        if ($request->has('poster')) {
            $path = 'images/';
            File::delete($path.$film ->poster);
            $fileName = time().'.'.$request->poster->extension();  
   
            $request->poster->move(public_path('images'), $fileName);

            $film -> poster= $fileName;
            $film -> save();

        }

        $film->judul=$request['judul'];
        $film->ringkasan=$request['ringkasan'];
        $film->tahun=$request['tahun'];
        $film->genre_id=$request['genre_id'];
        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $film = Film::find($id);
        $path = 'images/';
        File::delete($path.$film ->poster);

        $film ->delete();
        return redirect ('/film');
    }
}
