<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function index(){

        $iduser = Auth::id();

        $profile = Profile::where('user_id', $iduser)->first();

        return view('profile.update', ['profile'=>$profile]);
    }

    public function update(Request $request, $id){

        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamt' => 'required'
            
        ]);


        $profile = Profile::find($id);

        $profile->umur=$request['umur'];
        $profile->bio=$request['bio'];
        $profile->alamt=$request['alamt'];
        $profile->save();

        return redirect('/profile');
    }
}
