@extends('layout.master')

@section('title')
    
<h1>Halaman List Genre</h1>
@endsection
@section('content')
 <a href="/genre/create" class="btn btn-primary btn-sm mb-3">Tambah</a>



 <table class="table">
     <thead class="thead-light">
       <tr>
         <th scope="col">#</th>
         <th scope="col">Title</th>
         <th scope="col">Action</th>
       
       </tr>
     </thead>
     <tbody>
         @forelse ($genre as $key=>$isi)
             <tr>
                 <td>{{$key + 1}}</th>
                 <td>{{$isi->nama}}</td>
                 
                 
                 <td>
                     
                     <form action="/genre/{{$isi->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/genre/{{$isi->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/genre/{{$isi->id}}/edit" class="btn btn-danger btn-sm">Edit</a>
                    <input type="submit" value="delete" class="btn btn-warning btn-sm">
                    
                    </form>
                    
                 </td>
             </tr>
         @empty
             <tr >
                 <td>No data</td>
             </tr>  
         @endforelse              
     </tbody>
 </table>


@endsection