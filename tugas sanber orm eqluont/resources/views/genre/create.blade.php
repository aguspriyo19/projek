@extends('layout.master')

@section('title')
    
<h1>Halaman Create Genre</h1>
@endsection
@section('content')
 

<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama Genre</label>
      <input type="text" class="form-control" name="nama">
    </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
