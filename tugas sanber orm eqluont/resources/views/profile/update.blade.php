@extends('layout.master')

@section('title')
    
<h1>Halaman Update profile</h1>
@endsection
@section('content')
 

<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Umur</label>
      <input type="number" class="form-control" name="umur" value="{{old('umur',$profile->umur)}}">
    </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Biodata</label>
      <textarea name="bio" class="form-control mb-3" id="" cols="30" rows="10">{{old('bio',$profile->bio)}}</textarea>
      
    </div>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

    <div class="form-group">
      <label >Alamat</label>
    </div>
    <textarea name="alamt" class="form-control mb-3" id="" cols="30" rows="10">{{old('alamt',$profile->alamt)}}</textarea>
    @error('alamt')
     <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
