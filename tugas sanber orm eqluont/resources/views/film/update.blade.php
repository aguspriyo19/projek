@extends('layout.master')

@section('title')
    
<h1>Halaman Edit</h1>
@endsection
@section('content')
 

<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" class="form-control" value="{{$film ->judul}}" name="judul">
    </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Tahun Film</label>
      <input type="integer" class="form-control" value="{{$film ->tahun}}" name="tahun">
    </div>
        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Ringkasan Film</label>
    </div>
    <textarea name="ringkasan" class="form-control mb-3" id="" cols="30" rows="10">{{$film->ringkasan}}</textarea>
    @error('ringkasan')
     <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Poster</label>
        <input type="file" class="form-control" name="poster">
      </div>
          @error('poster')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <div class="form-group">
            <label>Genre</label>
            <select name="genre_id" class="form-control" id="">
                <option value=""> --Pilih Genre-- </option>
                @forelse ($genre as $item)
                @if ($item->id === $film ->genre_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                    
                @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
                @empty
            <option> Tidak ada data Genre</option>
                @endforelse
            </select>
          </div>
              @error('genre_id')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection

