@extends('layout.master')

@section('title')
    
<h1>Halaman Film</h1>
@endsection
@section('content')
 

<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" class="form-control" name="judul">
    </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Tahun Film</label>
      <input type="integer" class="form-control" name="tahun">
    </div>
        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Ringkasan Film</label>
    </div>
    <textarea name="ringkasan" class="form-control mb-3" id="" cols="30" rows="10"></textarea>
    @error('ringkasan')
     <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Poster</label>
        <input type="file" class="form-control" name="poster">
      </div>
          @error('poster')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <div class="form-group">
            <label>Genre</label>
            <select name="genre_id" class="form-control" id="">
                <option value=""> --Pilih Genre-- </option>
                @forelse ($genre as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @empty
            <option> Tidak ada data Genre</option>
                @endforelse
            </select>
          </div>
              @error('genre_id')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection

