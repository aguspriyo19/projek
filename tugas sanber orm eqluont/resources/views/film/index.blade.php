@extends('layout.master')

@section('title')
    
<h1>Halaman List Film</h1>
@endsection
@section('content')
<a href="/film/create" class="btn btn-primary btn-sm mb-4">Tambah Film</a>

<div class="row">
    @forelse ($film as $item)

     <div class="col-4">

            <div class="card" >
                <img src="{{asset('images/'.$item->poster)}}" class="card-img-top" alt="..." >
                <div class="card-body">
                    <h3 class="card-title">{{$item->judul}}</h3>
                    <p class="card-text">{{ Str::limit($item->ringkasan, 40) }}</p>
                    <p class="card-text">{{$item->tahun}}</p>
                    <a href="/film/{{$item->id}}" class="btn btn-success btn-block btn-sm">Read Me</a>
                    @auth
                        
                    <div class="row my-2">
                        <div class="col">
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning  btn-block btn-sm">Edit</a>
                        </div>
                        <div class="col">
                            <form method="post" action="/film/{{$item->id}}">
                                @csrf
                                @method('delete')
                                <input type="submit" class="btn btn-danger btn-block btn-sm" value="delete">
                                
                            </form>
                        </div>


                    </div>
                    @endauth
                </div>
            </div>
     </div>
        @empty
        
        <h2>Tidak ada daftar film</h2>
    @endforelse
</div>
@endsection