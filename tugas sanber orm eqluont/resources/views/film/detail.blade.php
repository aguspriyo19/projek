@extends('layout.master')

@section('title')
    
<h1>Halaman Detail Film</h1>
@endsection
@section('content')


    <img src="{{asset('images/'.$film->poster)}}" class="card-img-top" alt="..." >
   
        <h3 class="card-title">{{$film->judul}}</h3>
        <p class="card-text">{{ $film->ringkasan}}</p>
        <p class="card-text">{{$film->tahun}}</p>
        <a href="/film" class="btn btn-warning btn-block btn-sm">Kembali</a>
  


@endsection