@extends('layout.master')

@section('title')
    
<h1>Halaman Edit Cast</h1>
@endsection
@section('content')
 

<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
    </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Umur</label>
      <input type="integer" class="form-control" name="umur" value="{{$cast->umur}}"  >
    </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Bio</label>
     
    </div>
    <textarea name="bio" class="form-control mb-3" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
    @error('bio')
     <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
