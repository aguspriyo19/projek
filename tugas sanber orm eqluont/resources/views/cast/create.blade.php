@extends('layout.master')

@section('title')
    
<h1>Halaman Create Cast</h1>
@endsection
@section('content')
 

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama</label>
      <input type="text" class="form-control" name="nama">
    </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Umur</label>
      <input type="integer" class="form-control" name="umur">
    </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Bio</label>
     
    </div>
    <textarea name="bio" class="form-control mb-3" id="" cols="30" rows="10"></textarea>
    @error('bio')
     <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
