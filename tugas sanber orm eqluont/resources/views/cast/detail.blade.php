@extends('layout.master')

@section('title')
    
<h1>Detail Cast</h1>
@endsection


@section('content')
<h1>{{$cast->nama}}</h1>
<p>Umur: {{$cast->umur}}</p>
<p>Quotes: {{$cast->bio}}</p>

<a href="/cast" class="btn btn-info btn-sm">Kembali</a>
@endsection