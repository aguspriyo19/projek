<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }
    public function kirim(Request $request){
        // $namadepan = $request['fname'];
        // $namabelakang = $request['lname'];
        // return view('halaman.home', ['namadepan' => $namadepan, 'namabelakang'=>$namabelakang]);
        $fulname = $request ['fname']. ' ' .$request['lname'];
        return view('halaman.home',['fulname'=>$fulname]);
    }
}
