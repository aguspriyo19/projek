@extends('layout.master')

@section('title')
    
<h1>Buat Account Baru!</h1>
@endsection
@section('content')
    
<h2>Sign Up Form</h2>
<form action="/kirim" method="post">
@csrf
<label >First Name:</label> 
<br> <br>
<Input type="text" name="fname"> 
<br> <br>
<label >Last Name:</label> 
<br> <br>
<Input type="text" name="lname" > 
<br> <br>
<label for="gender">Gender:</label>
<br> <br>
<input type="radio" name="gender">Male
<br>
<input type="radio" name="gender">Female
<br> 
<input type="radio" name="gender">Other
<br><br>
<label for="national">Nationality:</label> <br> <br>
<select name="nationaliti" id="" >
    <option value="Indonesia">Indonesia</option>
    <option value="Other">Other</option>
</select>
<br> <br>
<label for="" >Language Spoken:</label>
<br><br>
<input type="checkbox" name="bahasa">Bahasa Indonesia  <br> 
<input type="checkbox" name="bahasa">English <br>  
<input type="checkbox" name="bahasa">Other   <br>
<br>
<label for="Bio">Bio:</label>
<br> <br>
<textarea name="message" id="" cols="30" rows="10"></textarea>
<br>

<button >Sign Up</button>
</form>
@endsection

