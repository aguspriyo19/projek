<?php

namespace App\Models;




class Post 
{
 private static $blog_posts =  [
    [
        "title"=>"Judul Post Pertama",
        "slug"=>"judul-post-pertama",
        "Author"=>"Agus Priyono",
        "body"=> "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum inventore iure officia minus blanditiis facere obcaecati in iste excepturi nesciunt! Ab eaque excepturi, aspernatur sed iure perferendis harum placeat veritatis commodi reprehenderit ex laboriosam officia pariatur quidem necessitatibus voluptatibus. Numquam, nam distinctio ex quo esse voluptate fugiat possimus quis ad suscipit. Expedita nobis esse soluta quibusdam? Atque ipsa ratione molestiae quae a inventore fugit tenetur reprehenderit iusto, quibusdam tempora? Necessitatibus aspernatur consectetur, perferendis obcaecati facilis voluptatibus doloribus unde adipisci delectus!"

     ],
     [
        "title"=>"Judul Post Kedua",
        "slug"=>"judul-post-kedua",
        "Author"=>"Dian yunita",
        "body"=> "Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt quas voluptatem corporis quasi molestiae aliquid pariatur, deserunt possimus doloribus? Dignissimos libero sapiente voluptatibus veniam eos culpa consequatur in provident quia, sunt repellendus aliquam veritatis neque nostrum deleniti iste perferendis nihil nobis ducimus ea deserunt repellat vitae. Veniam dolorum voluptates, itaque dolor praesentium, molestiae sit sint soluta modi dolore id esse voluptate? Enim consectetur similique numquam pariatur perferendis corrupti laudantium quas, placeat, excepturi debitis illum! Enim asperiores animi ad itaque, dolorum modi, tempora recusandae debitis dolores cumque non delectus? Aperiam fugiat corporis delectus ex ab saepe adipisci error minus obcaecati soluta!"
     ]  
   ];

   public static function all()
   {
    return collect( self:: $blog_posts);
   }

   public static function find($slug)
   {
    $posts = static::all();
return $posts->firstWhere('slug',$slug);
   }
}
