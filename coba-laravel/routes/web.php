<?php

use App\Http\Controllers\PostController;
use App\Models\Post;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home',[
        "title"=> "Home"
    ]);
});
Route::get('/about', function () {
    return view('pages.about', [
        "title"=>"About",
        "name"=>"Agus Priyono",
        "email"=>"agusp.tkr3.04@gmail.com",
        "image"=>"php.png"
    ]);
});




Route::get('/posts', [PostController::class, 'index']);


Route::get('pages.posts/{slug}', [PostController::class, 'show']);